databaseChangeLog = {

	changeSet(author: "kmikolajczyk (generated)", id: "1416870318666-1") {
		createTable(tableName: "px_role") {
			column(name: "px_id", type: "int8") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "px_rolePK")
			}

			column(name: "px_version", type: "int8") {
				constraints(nullable: "false")
			}

			column(name: "px_authority", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "kmikolajczyk (generated)", id: "1416870318666-2") {
		createTable(tableName: "px_user") {
			column(name: "px_id", type: "int8") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "px_userPK")
			}

			column(name: "px_version", type: "int8") {
				constraints(nullable: "false")
			}

			column(name: "px_account_expired", type: "boolean") {
				constraints(nullable: "false")
			}

			column(name: "px_account_locked", type: "boolean") {
				constraints(nullable: "false")
			}

			column(name: "px_enabled", type: "boolean") {
				constraints(nullable: "false")
			}

			column(name: "password", type: "varchar(255)") {
				constraints(nullable: "false")
			}

			column(name: "px_password_expired", type: "boolean") {
				constraints(nullable: "false")
			}

			column(name: "px_username", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "kmikolajczyk (generated)", id: "1416870318666-3") {
		createTable(tableName: "px_user_role") {
			column(name: "px_role_id", type: "int8") {
				constraints(nullable: "false")
			}

			column(name: "px_user_id", type: "int8") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "kmikolajczyk (generated)", id: "1416870318666-4") {
		addPrimaryKey(columnNames: "px_role_id, px_user_id", constraintName: "px_user_rolePK", tableName: "px_user_role")
	}

	changeSet(author: "kmikolajczyk (generated)", id: "1416870318666-7") {
		createIndex(indexName: "px_authority_uniq_1416870318482", tableName: "px_role", unique: "true") {
			column(name: "px_authority")
		}
	}

	changeSet(author: "kmikolajczyk (generated)", id: "1416870318666-8") {
		createIndex(indexName: "px_username_uniq_1416870318489", tableName: "px_user", unique: "true") {
			column(name: "px_username")
		}
	}

	changeSet(author: "kmikolajczyk (generated)", id: "1416870318666-9") {
		createSequence(sequenceName: "seq_px_role")
	}

	changeSet(author: "kmikolajczyk (generated)", id: "1416870318666-10") {
		createSequence(sequenceName: "seq_px_user")
	}

	changeSet(author: "kmikolajczyk (generated)", id: "1416870318666-5") {
		addForeignKeyConstraint(baseColumnNames: "px_role_id", baseTableName: "px_user_role", constraintName: "FK_trg2408uko4w32x23408kw717", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "px_id", referencedTableName: "px_role", referencesUniqueColumn: "false")
	}

	changeSet(author: "kmikolajczyk (generated)", id: "1416870318666-6") {
		addForeignKeyConstraint(baseColumnNames: "px_user_id", baseTableName: "px_user_role", constraintName: "FK_jw02pp17or3md0me2qpeqppus", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "px_id", referencedTableName: "px_user", referencesUniqueColumn: "false")
	}
}
