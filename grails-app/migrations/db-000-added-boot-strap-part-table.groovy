databaseChangeLog = {

	changeSet(author: "kmikolajczyk (generated)", id: "1416866427964-1") {
		createTable(tableName: "px_boot_strap_part") {
			column(name: "px_id", type: "int8") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "px_boot_strapPK")
			}

			column(name: "px_version", type: "int8") {
				constraints(nullable: "false")
			}

			column(name: "px_key", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "kmikolajczyk (generated)", id: "1416866427964-2") {
		createSequence(sequenceName: "seq_px_boot_strap_part")
	}
}
