package pl.pxt8.bootstrap

import static BootStrapPartName.*
import grails.transaction.Transactional
import grails.util.Environment
import org.codehaus.groovy.grails.commons.GrailsApplication
import pl.pxt8.security.Role
import pl.pxt8.security.User

@Transactional
class BootStrapService {

    GrailsApplication grailsApplication
//    FileDomainStorageService fileDomainStorageService

//    def pxt8SchedulerService

    static transactional = false

    @Transactional
    void boot(Environment environment) {
        switch (environment) {
            case Environment.PRODUCTION:
                bootProduction()
                break
            case Environment.DEVELOPMENT:
                //enable schedulers in development mode (assume no loadbalancing)
//                println 'Enabling cron scheduler in development mode!'
//                System.setProperty('pxt8.enableCronScheduler','true')
                bootDevelopment()
                break
            case Environment.TEST:
                bootTest()
                break
        }
    }

    void startup(Environment environment) {
        switch (environment) {
            case Environment.PRODUCTION:
                startupSchedulers()
                break
            case Environment.DEVELOPMENT:
                startupSchedulers()
                break
        }
    }

    void startupSchedulers() {
//        pxt8SchedulerService.startup()
        log.info 'Schedulers started up'
    }

    void shutdown() {
//        pxt8SchedulerService.shutdown()
    }

    void bootProduction() {
        defaults()
        applicationFiles()
        productionData()
        migrations()
        checkConfigs()
    }

    void checkConfigs() {
        checkAdminPasswords()
    }

    void checkAdminPasswords() {
        if (!grailsApplication.config.pxt8.passwords.allowDevDefaultPassword) {
            if (User.DEV_DEFAULT_PASSWORD == grailsApplication.config.pxt8.passwords.admin || User.DEV_DEFAULT_PASSWORD == grailsApplication.config.pxt8.passwords.superAdmin) {
                throw new Exception("ADMINS PASSWORDS SHOULD BE CHANGED IN PRODUCTION ENVIRONMENT")
            }
        }
    }

    void bootDevelopment() {
        defaults()
        applicationFiles()
        developmentData()
        migrations()
    }

    void bootTest() {
        defaults()
    }

    /**
     * Here the default objects should be created in proper default methods.
     *
     *  Note: it does NOT use createBootstrapPart() method to simplify unit testing
     */
    private void defaults() {
        defaultSecurity()
    }

    /**
     * Here the resource files are preloaded and ApplicationFileInfo's are stored in database
     *
     *  Note: it does NOT use createBootstrapPart() method to simplify unit testing
     */
    private void applicationFiles() {
    }
//
//    /**
//     * Here the default roles and users are created
//     */
    private void defaultSecurity() {
        createBootstrapPart(SECURITY) {
            defaultRoles()

            User.createUser('juzek', grailsApplication.config.pxt8.passwords.juzek)
            User.createAdmin('admin', grailsApplication.config.pxt8.passwords.admin)
            User.createSuper('super', grailsApplication.config.pxt8.passwords.superAdmin)
        }
    }

    private void defaultRoles() {
        createBootstrapPart(ROLES) {
            Role.createDefaults()
        }
    }

    /**
     * Here basic test data for development is created
     *
     *  Note: it does NOT use createBootstrapPart() method to simplify unit testing
     */
    private void developmentData() {

    }

    /**
     * Here basic test data for production is created
     *
     *  Note: it does NOT use createBootstrapPart() method to simplify unit testing
     */
    private void productionData() {

    }

    void migrations() {

    }

    private void createBootstrapPart(BootStrapPartName label, Closure body) {
        String line = "processing [$label] -> "
        if(!BootStrapPart.exist(label)) {
            body()
            BootStrapPart.add(label)
            line += " creating"
        } else {
            line += " already in db"
        }
        log.info(line)
    }
}
