package pl.pxt8

//import grails.plugins.springsecurity.SpringSecurityService

class TraceFilters {
//    SpringSecurityService springSecurityService

    def filters = {
        trace(controller:'*', action:'*') {
            before = {
                try {
                    if (controllerName != 'file') {
//                        User user = springSecurityService.getCurrentUser()
//                        String login = user ? user.username : 'guest'
                        String login = 'guest'
                        String ip = request == null ? "can't get remote address" : request.getRemoteAddr()
                        Map parameters = params.clone()
                        parameters.each {
                            if(it.key.endsWith('assword') || it.key.endsWith('asswordConfirmation')) {
                                it.value = '*'
                            }
                        }
                        if(!(controllerName == 'assets')) {
                            log.info("[${login}]: ${controllerName}/${actionName} - $parameters <- $ip")
                        }
                    }
                } catch(Exception ex) {
                    log.error(ex)
                }
            }
        }
    }
}

