import grails.util.Environment
import org.codehaus.groovy.grails.commons.GrailsApplication
import pl.pxt8.bootstrap.BootStrapService

class BootStrap {

    /**
     * {@link org.codehaus.groovy.grails.commons.GrailsApplication}
     */
    GrailsApplication grailsApplication

    /**
     * {@link BootStrapService}
     */
    BootStrapService bootStrapService
    /**
     * This method is initial point of application
     */
    def init = { servletContext ->
        String bootMessage = 'Bootstrap success'
        try {
            bootStrapService.boot(Environment.current)
            log.info('Bootstrap success')
            bootStrapService.startup(Environment.current)
        } catch(Exception e) {
            bootMessage = 'Bootstrap failed'
            log.error("Bootstrap failed, ${e}")
            e.printStackTrace()
        } finally {
//            String instanceName = System.getProperty('instanceName')?.trim()
//            if(instanceName) {
//                File dir = new File(grailsApplication.config.pxt8.bootstrapLockDir)
//                if(!dir.exists()) {
//                    dir.mkdirs()
//                }
//                File file = new File(dir, instanceName)
//                file.text = bootMessage + '\n'
//            }
        }
    }

    def destroy = {
        try {
            bootStrapService.shutdown()
        } catch(Exception e) {
            log.error("Destroy failed, ${e}")
        }
    }
}
