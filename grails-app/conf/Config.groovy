import pl.pxt8.security.User

// locations to search for config files that get merged into the main config
// config files can either be Java properties files or ConfigSlurper scripts

String APP_HOME = "${System.getProperty('user.home')}/.${appName}"
String TMP_DIR = 'tmpdir'

environments {
    test {
        TMP_DIR = 'tmpdir-test'
    }
}
System.setProperty("java.io.tmpdir", "${APP_HOME}/${TMP_DIR}")

File tmpDirectory = new File(System.getProperty("java.io.tmpdir"))
if(!tmpDirectory.exists()) {
    tmpDirectory.mkdirs()
}

grails.config.locations = []
//grails.config.locations << CaptchaConfig // added captcha configuration file
//grails.config.locations << [" classpath:conf/Quartz.properties" ]

if(new File("${APP_HOME}/Config.groovy").exists()) {
    println "Including configuration file specified in user home - ${APP_HOME}/Config.groovy"
    grails.config.locations << "file:${APP_HOME}/Config.groovy"
} else {
    println "No external configuration file defined, running with default values."
}

grails.project.groupId = "pl.pxt8"

// The ACCEPT header will not be used for content negotiation for user agents containing the following strings (defaults to the 4 major rendering engines)
grails.mime.disable.accept.header.userAgents = ['Gecko', 'WebKit', 'Presto', 'Trident']
grails.mime.types = [ // the first one is the default format
    all:           '*/*', // 'all' maps to '*' or the first available format in withFormat
    atom:          'application/atom+xml',
    css:           'text/css',
    csv:           'text/csv',
    form:          'application/x-www-form-urlencoded',
    html:          ['text/html','application/xhtml+xml'],
    js:            'text/javascript',
    json:          ['application/json', 'text/json'],
    multipartForm: 'multipart/form-data',
    rss:           'application/rss+xml',
    text:          'text/plain',
    hal:           ['application/hal+json','application/hal+xml'],
    xml:           ['text/xml', 'application/xml']
]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// Legacy setting for codec used to encode data with ${}
grails.views.default.codec = "html" // none, html, base64

// The default scope for controllers. May be prototype, session or singleton.
// If unspecified, controllers are prototype scoped.
grails.controllers.defaultScope = 'singleton'

// GSP settings
grails {
    views {
        gsp {
            encoding = 'UTF-8'
            htmlcodec = 'xml' // use xml escaping instead of HTML4 escaping
            codecs {
                expression = 'html' // escapes values inside ${}
                scriptlet = 'html' // escapes output from scriptlets in GSPs
                taglib = 'none' // escapes output from taglibs
                staticparts = 'none' // escapes output from static template parts
            }
        }
        // escapes all not-encoded output at final stage of outputting
        // filteringCodecForContentType.'text/html' = 'html'
    }
}


grails.converters.encoding = "UTF-8"
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart=false

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

// configure auto-caching of queries by default (if false you can cache individual queries with 'cache: true')
grails.hibernate.cache.queries = false

// configure passing transaction's read-only attribute to Hibernate session, queries and criterias
// set "singleSession = false" OSIV mode in hibernate configuration after enabling
grails.hibernate.pass.readonly = false
// configure passing read-only to OSIV session by default, requires "singleSession = false" OSIV mode
grails.hibernate.osiv.readonly = false

// When set to true the save method with throw a grails.validation.ValidationException if validation fails during a save.
grails.gorm.failOnError = true

environments {
    development {
        grails.logging.jul.usebridge = true
    }
    production {
        grails.logging.jul.usebridge = false
    }
}

String logPath = "${APP_HOME}/${appName}.log"
String instanceName = System.getProperty('instanceName')?.trim()?:'DEV'

// log4j configuration
log4j.main = {
    appenders {
        rollingFile name:'file', file: logPath, layout:pattern(conversionPattern: " %d |${instanceName}| |%p| %c{1}: %m%n"), maxFileSize:"25MB", maxBackupIndex: 1024
    }
    root {
        info 'file', 'stdout'
    }

    // Turn on this if you want to enable more detailed log from grails application
//    debug  'grails.app'

    error  'org.codehaus.groovy.grails.web.servlet',        // controllers
           'org.codehaus.groovy.grails.web.pages',          // GSP
           'org.codehaus.groovy.grails.web.sitemesh',       // layouts
           'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
           'org.codehaus.groovy.grails.web.mapping',        // URL mapping
           'org.codehaus.groovy.grails.commons',            // core / classloading
           'org.codehaus.groovy.grails.plugins',            // plugins
           'org.codehaus.groovy.grails.orm.hibernate',      // hibernate integration
           'org.springframework',
           'org.hibernate',
           'net.sf.ehcache.hibernate',
           'grails.util.GrailsUtil'
    warn   'org.mortbay.log',
           'grails.app.service.grails.buildtestdata.BuildTestDataService',
           'grails.buildtestdata.DomainInstanceBuilder',
           'com.dalew'

}

// Database migration configuration
grails.plugin.databasemigration.changelogLocation = "grails-app/migrations"
grails.plugin.databasemigration.updateOnStart = true
grails.plugin.databasemigration.updateOnStartFileNames = ['changelog.groovy']

// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'pl.pxt8.security.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'pl.pxt8.security.UserRole'
grails.plugin.springsecurity.authority.className = 'pl.pxt8.security.Role'
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
	'/':                              ['permitAll'],
	'/index':                         ['permitAll'],
	'/index.gsp':                     ['permitAll'],
    '/**/assets/**':                  ['permitAll'],
    '/**/js/**':                      ['permitAll'],
	'/**/css/**':                     ['permitAll'],
	'/**/images/**':                  ['permitAll'],
	'/**/favicon.ico':                ['permitAll']
]



pxt8 {
    passwords {
        juzek = User.DEV_DEFAULT_PASSWORD
        admin = User.DEV_DEFAULT_PASSWORD
        superAdmin = User.DEV_DEFAULT_PASSWORD
        allowDevDefaultPassword = true
    }
}

environments {
    production {
        pxt8.passwords.allowDevDefaultPassword = false
    }
}