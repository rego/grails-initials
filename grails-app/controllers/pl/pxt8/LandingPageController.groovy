package pl.pxt8

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import pl.pxt8.security.User

@Secured("isAnonymous()")
class LandingPageController {

    SpringSecurityService springSecurityService

    def index() {
        User loggedInUser = springSecurityService.currentUser
        if (loggedInUser) {
            if (loggedInUser.hasRole("ROLE_ADMIN")) {
                return redirect(controller: "adminHome")
            } else if (loggedInUser.hasRole("ROLE_USER")) {
                return redirect(controller: "userHome")
            }
        }
    }
}
