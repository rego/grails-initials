package pl.pxt8.bootstrap

/**
 * This table is for database versioning. Each key represents data that is
 * injected to database in bootstraping process.
 * We can inject more default/custom data after deploy.
 *
 * @author Grzegorz Gajos
 * @author <a href="mailto:k4mil.mikolajczyk@gmail.com">Kamil Mikolajczyk</a>
 */
class BootStrapPart {

    BootStrapPartName key

    /**
     * Check if specified key exist in database
     * @param key key
     * @return true if exist
     */
    static boolean exist(BootStrapPartName key) {
        findByKey(key) != null
    }

    /**
     * Adds key to database
     * @param key key
     */
    static void add(BootStrapPartName key) {
        new BootStrapPart(key:key).save()
    }
}