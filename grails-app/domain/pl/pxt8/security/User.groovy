package pl.pxt8.security

import org.apache.commons.lang.NotImplementedException

class User {

	static final String DEV_DEFAULT_PASSWORD = '!234Qwer'

	transient springSecurityService

	String username
	String password
	boolean enabled = true
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired

	static transients = ['springSecurityService']

	static constraints = {
		username blank: false, unique: true
		password blank: false
	}

	static mapping = {
		password column: '`password`'
	}

	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this).collect { it.role }
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
	}

	boolean hasRole(Role role) {
		UserRole.findByRoleAndUser(role, this) != null
	}

	boolean hasRole(String authority) {
		UserRole.createCriteria().get {
			eq 'user', this
			role {
				eq 'authority', authority
			}
			projections {
				count()
			}
		} > 0
	}

	void addRole(Role role) {
		if (!hasRole(role)) {
			new UserRole(user: this, role: role).save()
		}
	}

	void addRole(String auth) {
		Role role = Role.findByAuthority(auth)
		if (!role) {
			throw new Exception("Role [$auth] not found")
		}
		addRole(role)
	}

	static User createSuper(String username, String password) {
		createUser([
				username: username,
				password: password,
				enabled: true
		],
				[
						'ROLE_USER',
						'ROLE_SUPERADMIN',
						'ROLE_ADMIN'
				]
		)
	}

	static User createAdmin(String username, String password) {
		createUser([
				username: username,
				password: password,
				enabled: true
		],
				[
						'ROLE_USER',
						'ROLE_ADMIN'
				]
		)
	}

	static User createUser(String username, String password) {
		createUser([
				username: username,
				password: password,
				enabled: true
		],
				[
						'ROLE_USER'
				]
		)
	}

	static User createUser(Map props, List authorities) {
		User user = new User(props).save()
		authorities.each { String auth ->
			user.addRole(auth)
		}
		return user
	}
}
