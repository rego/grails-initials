package pl.pxt8.security

class Role {

	String authority

	static mapping = {
		cache true
	}

	static constraints = {
		authority blank: false, unique: true
	}

	static public void createDefaults() {
		withTransaction {
			['ROLE_USER', 'ROLE_ADMIN', 'ROLE_SUPERADMIN'].each {
				new Role(authority: it).save()
			}
		}
	}
}
