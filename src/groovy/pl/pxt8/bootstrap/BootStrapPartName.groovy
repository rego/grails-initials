package pl.pxt8.bootstrap

/**
 * keys for Bootstrap table
 * @author <a href="mailto:k4mil.mikolajczyk@gmail.com">Kamil Mikolajczyk</a>
 */
public enum BootStrapPartName {
    SECURITY,
    ROLES
}