package pl.pxt8.configs

import org.hibernate.cfg.ImprovedNamingStrategy

/**
 * Implementation of naming strategy. <br/> 
 * Add "px_" prefix to all pxf tables, and "px_" to all collumns
 * <b>We need ths because of restrictions by databases/sql/hsql keywords...</b>
 * @author <a href="mailto:lukasz.grzesik@itsilesia.com">Lukasz.Grzesik@itSilesia.com</a>
 */
class NamingStrategy extends ImprovedNamingStrategy {

    /* (non-Javadoc)
     * @see org.hibernate.cfg.ImprovedNamingStrategy#classToTableName(java.lang.String)
     */
    String classToTableName(String className) {
        "px_" + super.classToTableName(className)
    }

    /* (non-Javadoc)
     * @see org.hibernate.cfg.ImprovedNamingStrategy#propertyToColumnName(java.lang.String)
     */
    String propertyToColumnName(String propertyName) {
        "px_" + super.propertyToColumnName(propertyName)
    }
}