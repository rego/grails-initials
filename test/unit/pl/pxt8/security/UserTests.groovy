package pl.pxt8.security

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import org.junit.Test

@TestFor(User)
@Mock([Role, UserRole])
class UserTests {

    @Test
    void hasRoleByAuthority() {
        // given
        createExampleRolesAndUsers()
        User u1 = User.findByUsername('u1')
        User u2 = User.findByUsername('u2')
        Role r1 = Role.findByAuthority('ROLE_R1')
        Role r2 = Role.findByAuthority('ROLE_R2')
        Role r3 = Role.findByAuthority('ROLE_R3')
        Role r4 = Role.findByAuthority('ROLE_R4')

        new UserRole(user: u1, role: r1).save()
        new UserRole(user: u1, role: r2).save()
        new UserRole(user: u2, role: r2).save()
        new UserRole(user: u2, role: r3).save(flush: true)

        // when
        boolean u1HasR1 = u1.hasRole('ROLE_R1')
        boolean u1HasR2 = u1.hasRole('ROLE_R2')
        boolean u1HasNoR3 = u1.hasRole('ROLE_R3')
        boolean u1HasNoR4 = u1.hasRole('ROLE_R4')
        boolean u2HasNoR1 = u2.hasRole('ROLE_R1')
        boolean u2HasR2 = u2.hasRole('ROLE_R2')
        boolean u2HasR3 = u2.hasRole('ROLE_R3')
        boolean u2HasNoR4 = u2.hasRole('ROLE_R4')
        boolean u2HasNoNotExistingRole = u2.hasRole('ROLE_NotExistingRole')

        // then
        assert u1HasR1
        assert u1HasR2
        assert !u1HasNoR3
        assert !u1HasNoR4
        assert !u2HasNoR1
        assert u2HasR2
        assert u2HasR3
        assert !u2HasNoR4
        assert !u2HasNoNotExistingRole
    }

    private void createExampleRolesAndUsers() {
        new Role(authority: 'ROLE_R1').save()
        new Role(authority: 'ROLE_R2').save()
        new Role(authority: 'ROLE_R3').save()
        new Role(authority: 'ROLE_R4').save()
        new User(username: 'u1', password: 'p1').save()
        new User(username: 'u2', password: 'p2').save()
    }
}
